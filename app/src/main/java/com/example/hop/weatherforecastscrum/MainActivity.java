package com.example.hop.weatherforecastscrum;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_SERIALIZABLE_OBJECT = "forecast";
    public static final String KEY_BUNDLE = "bundle";
    private static final String KEY_SHARED_PREFERENCES = "WeatherForecast";
    private static final String KEY_CITY_NAME = "cityname";

    private List<Forecast> mForecasts;
    private ForecastAdapter mAdapter;
    private SharedPreferences mSharedPrefs;
    private ListView mLvForecast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.ic_launcher);

        mLvForecast = (ListView) findViewById(R.id.lvForecast);
        mForecasts = new ArrayList<>();

        mLvForecast.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                Forecast forecast = mForecasts.get(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable(KEY_SERIALIZABLE_OBJECT, forecast);
                intent.putExtra(KEY_BUNDLE, bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //check network
        if (!isNetworkConnected()){
            if (mForecasts.size() == 0){
                Toast.makeText(this, "Please enable network connection", Toast.LENGTH_LONG).show();
            }
            return;
        }

        mAdapter = new ForecastAdapter(this, mForecasts);
        mLvForecast.setAdapter(mAdapter);

        fetchData();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo == null){
            return false;
        }
        return true;
    }

    private void doSearch(String cityName) {
        if (!isNetworkConnected()){
            Toast.makeText(this, "Please enable network connection", Toast.LENGTH_LONG).show();
            return;
        }
        LoadDataAsyncTask task = new LoadDataAsyncTask(this, mForecasts, mAdapter);
        task.execute(cityName);
    }

    private void fetchData() {
        mSharedPrefs = getSharedPreferences(KEY_SHARED_PREFERENCES, MODE_PRIVATE);
        String cityName = mSharedPrefs.getString(KEY_CITY_NAME, getString(R.string.default_cityName));
        doSearch(cityName);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                //Here u can get the value "query" which is entered in the search box.
                doSearch(query);
                MenuItem menuItem = menu.findItem(R.id.search);
                menuItem.collapseActionView();

                SharedPreferences.Editor editor = mSharedPrefs.edit();
                editor.putString(KEY_CITY_NAME, query);
                editor.commit();
                return true;
            }

        };
        searchView.setOnQueryTextListener(queryTextListener);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


}
