package com.example.hop.weatherforecastscrum;

/**
 * Created by Hop on 22/10/2015.
 */
public class Config {
    public static final String FORECAST_BASE_URL =
            "http://api.openweathermap.org/data/2.5/forecast/daily?";
    public static final String  QUERY_PARAM = "q";
    public static final String FORMAT_PARAM = "mode";
    public static final String UNITS_PARAM = "units";
    public static final String DAYS_PARAM = "cnt";
    public static final String APPID_PARAM = "APPID";
    public static final String OPEN_WEATHER_MAP_API_KEY = "e9ee8c9c1a9c1699921eda32fa986955";

    public static final String DEFAULT_FORMAT = "json";
    public static final String DEFAULT_UNITS = "metric";
    public static final int DEFAULT_CNT = 7;


}
