package com.example.hop.weatherforecastscrum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

public class ForecastAdapter extends BaseAdapter {
    private static final int VIEW_TYPE_COUNT = 2;
    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;
    private static final int TODAY_POSITION = 0;

    private Context mContext;
    private List<Forecast> mForecasts;

    public ForecastAdapter(Context context, List<Forecast> forecasts) {
        mContext = context;
        mForecasts = forecasts;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == TODAY_POSITION) {
            return VIEW_TYPE_TODAY;
        } else {
            return VIEW_TYPE_FUTURE_DAY;
        }
    }

    @Override
    public int getCount() {
        return mForecasts.size();
    }

    @Override
    public Forecast getItem(int position) {
        return mForecasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        public final ImageView iconView;
        public final TextView dateView;
        public final TextView descriptionView;
        public final TextView highTempView;
        public final TextView lowTempView;

        public ViewHolder(View view) {
            iconView = (ImageView) view.findViewById(R.id.list_item_icon);
            dateView = (TextView) view.findViewById(R.id.list_item_date_textview);
            descriptionView = (TextView) view.findViewById(R.id.list_item_forecast_textview);
            highTempView = (TextView) view.findViewById(R.id.list_item_high_textview);
            lowTempView = (TextView) view.findViewById(R.id.list_item_low_textview);
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        //Choose the layout type
        int viewType = getItemViewType(position);
        int layoutId = -1;
        switch (viewType) {
            case VIEW_TYPE_TODAY:
                layoutId = R.layout.item_list_forecast_today;
                break;
            case VIEW_TYPE_FUTURE_DAY:
                layoutId = R.layout.item_list_forecast;
                break;
        }

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(layoutId, parent, false);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        setValue(holder, position);

        return convertView;
    }

    private void setValue(ViewHolder holder, int position) {
        Forecast forecast = getItem(position);

        int viewType = getItemViewType(position);
        switch (viewType) {
            case VIEW_TYPE_TODAY: {
                // Get weather icon
                holder.iconView.setImageResource(Utility.getArtResourceForWeatherCondition(forecast.getWeatherId()));
                break;
            }
            case VIEW_TYPE_FUTURE_DAY: {
                // Get weather icon
                holder.iconView.setImageResource(Utility.getIconResourceForWeatherCondition(forecast.getWeatherId()));
                break;
            }
        }

        holder.dateView.setText(Utility.getFriendlyDayString(mContext, forecast.getDateTime()));
        holder.descriptionView.setText(forecast.getDescription());
        holder.iconView.setContentDescription(forecast.getDescription());
        holder.lowTempView.setText(forecast.getLow() + "\u00B0" + "C");
        holder.highTempView.setText(forecast.getHigh() + "\u00B0" + "C");
    }

}
