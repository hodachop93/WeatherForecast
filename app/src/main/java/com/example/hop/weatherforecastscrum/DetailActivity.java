package com.example.hop.weatherforecastscrum;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {

    private ImageView imgIconView;
    private TextView tvFriendlyDateView;
    private TextView tvDateView;
    private TextView tvDescriptionView;
    private TextView tvHighTempView;
    private TextView tvLowTempView;
    private TextView tvHumidityView;
    private TextView tvWindView;
    private TextView tvPressureView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);



        imgIconView= (ImageView) findViewById(R.id.detail_icon);
        tvDateView= (TextView) findViewById(R.id.detail_date_textview);
        tvFriendlyDateView= (TextView) findViewById(R.id.detail_day_textview);
        tvDescriptionView= (TextView) findViewById(R.id.detail_forecast_textview);
        tvHighTempView= (TextView) findViewById(R.id.detail_high_textview);
        tvLowTempView= (TextView) findViewById(R.id.detail_low_textview);
        tvHumidityView= (TextView) findViewById(R.id.detail_humidity_textview);
        tvWindView= (TextView) findViewById(R.id.detail_wind_textview);
        tvPressureView= (TextView) findViewById(R.id.detail_pressure_textview);

        Intent callerIntent = getIntent();
        Bundle receivedBundle = callerIntent.getBundleExtra(MainActivity.KEY_BUNDLE);
        Forecast forecast = (Forecast) receivedBundle.get(MainActivity.KEY_SERIALIZABLE_OBJECT);
        showData(forecast);
    }



    private void showData(Forecast forecast) {
        imgIconView.setImageResource(Utility.getArtResourceForWeatherCondition(forecast.getWeatherId()));
        imgIconView.setContentDescription(forecast.getDescription());
        tvDateView.setText(Utility.getFormattedMonthDay(this, forecast.getDateTime()));
        tvFriendlyDateView.setText(Utility.getFriendlyDayString(this, forecast.getDateTime()));
        tvDescriptionView.setText(forecast.getDescription());
        tvHighTempView.setText(forecast.getHigh() + "\u00B0" + "C");
        tvLowTempView.setText(forecast.getLow() + "\u00B0" + "C");
        tvHumidityView.setText(getString(R.string.format_humidity, forecast.getHumidity()));
        tvWindView.setText(getString(R.string.format_wind, forecast.getWindSpeed()));
        tvPressureView.setText(getString(R.string.format_pressure, forecast.getPressure()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
