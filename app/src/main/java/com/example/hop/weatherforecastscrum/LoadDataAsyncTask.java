package com.example.hop.weatherforecastscrum;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Time;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

/**
 * Created by Hop on 22/10/2015.
 */
public class LoadDataAsyncTask extends AsyncTask<String, Void, Void> {
    private Context mContext;
    private List<Forecast> mForecasts;
    private ForecastAdapter mAdapter;

    private String mCityName;
    public LoadDataAsyncTask(Context mContext, List<Forecast> mForecasts, ForecastAdapter mAdapter) {
        this.mContext = mContext;
        this.mForecasts = mForecasts;
        this.mAdapter = mAdapter;
    }

    @Override
    protected Void doInBackground(String... params) {
        String cityName = params[0];

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String forecastJsonStr = null;

        try {
            Uri builtUri = Uri.parse(Config.FORECAST_BASE_URL).buildUpon()
                    .appendQueryParameter(Config.QUERY_PARAM, cityName)
                    .appendQueryParameter(Config.FORMAT_PARAM, Config.DEFAULT_FORMAT)
                    .appendQueryParameter(Config.UNITS_PARAM, Config.DEFAULT_UNITS)
                    .appendQueryParameter(Config.DAYS_PARAM, Integer.toString(Config.DEFAULT_CNT))
                    .appendQueryParameter(Config.APPID_PARAM, Config.OPEN_WEATHER_MAP_API_KEY)
                    .build();

            URL url = new URL(builtUri.toString());

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            forecastJsonStr = buffer.toString();
            getDataFromJsonString(forecastJsonStr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void getDataFromJsonString(String forecastJsonStr) throws JSONException {
        final String KEY_CITY = "city";
        final String KEY_LIST = "list";
        final String KEY_CITY_NAME = "name";
        final String KEY_PRESSURE = "pressure";
        final String KEY_HUMIDITY = "humidity";
        final String KEY_WINDSPEED = "speed";
        final String KEY_WEATHER = "weather";
        final String KEY_DESCRIPTION = "description";
        final String KEY_WEATHER_ID = "id";
        final String KEY_TEMPERATURE = "temp";
        final String KEY_MAX = "max";
        final String KEY_MIN = "min";


        JSONObject rootObj = new JSONObject(forecastJsonStr);

        JSONObject cityObj = rootObj.getJSONObject(KEY_CITY);
        mCityName = cityObj.getString(KEY_CITY_NAME);

        JSONArray weatherArray = rootObj.getJSONArray(KEY_LIST);

        Time dayTime = new Time();
        dayTime.setToNow();

        // we start at the day returned by local time. Otherwise this is a mess.
        int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);

        // now we work exclusively in UTC
        dayTime = new Time();
        mForecasts.clear();
        for (int i = 0; i < weatherArray.length(); i++) {
            //Attribute
            long dateTime;
            double pressure;
            int humidity;
            double windSpeed;
            double high;
            double low;
            String description;
            int weatherId;

            // Get the JSON object representing the day
            JSONObject dayForecast = weatherArray.getJSONObject(i);

            // Cheating to convert this to UTC time, which is what we want anyhow
            dateTime = dayTime.setJulianDay(julianStartDay + i);

            pressure = dayForecast.getDouble(KEY_PRESSURE);
            humidity = dayForecast.getInt(KEY_HUMIDITY);
            windSpeed = dayForecast.getDouble(KEY_WINDSPEED);

            // Description is in a child array called "weather", which is 1 element long.
            // That element also contains a weather code.
            JSONObject weatherObject =
                    dayForecast.getJSONArray(KEY_WEATHER).getJSONObject(0);
            description = weatherObject.getString(KEY_DESCRIPTION);
            weatherId = weatherObject.getInt(KEY_WEATHER_ID);

            // Temperatures are in a child object called "temp".  Try not to name variables
            // "temp" when working with temperature.  It confuses everybody.
            JSONObject temperatureObject = dayForecast.getJSONObject(KEY_TEMPERATURE);
            high = temperatureObject.getDouble(KEY_MAX);
            low = temperatureObject.getDouble(KEY_MIN);


            Forecast forecast = new Forecast();
            forecast.setDateTime(dateTime);
            forecast.setPressure(pressure);
            forecast.setHumidity(humidity);
            forecast.setWindSpeed(windSpeed);
            forecast.setHigh(high);
            forecast.setLow(low);
            forecast.setDescription(description);
            forecast.setWeatherId(weatherId);

            mForecasts.add(forecast);
        }

    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mAdapter.notifyDataSetChanged();
        AppCompatActivity activity = (AppCompatActivity) mContext;
        activity.getSupportActionBar().setTitle(mCityName);
    }
}
