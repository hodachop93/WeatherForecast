Command line instructions


Git global setup

git config --global user.name "hodachop93"
git config --global user.email "hodachop93@gmail.com"

Create a new repository

git clone https://gitlab.com/hodachop93/WeatherForecast.git
cd WeatherForecast
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin https://gitlab.com/hodachop93/WeatherForecast.git
git add .
git commit
git push -u origin master